import React from "react";
import {Home} from "./pages/Initial/Home";
import {GettingStarted} from "./pages/Initial/GettingStarted";
import {ColumnsPage} from "./pages/Columns/ColumnsPage";
import {ColumnPage} from "./pages/Columns/ColumnPage";
import {LevelPage} from "./pages/Layout/LevelPage";
import {MediaObjectPage} from "./pages/Layout/MediaObjectPage";
import {ContainerPage} from "./pages/Layout/ContainerPage";
import {HeroPage} from "./pages/Layout/HeroPage";
import {SectionPage} from "./pages/Layout/SectionPage";
import {FooterPage} from "./pages/Layout/FooterPage";
import {TilePage} from "./pages/Layout/TilePage";
import {InputPage} from "./pages/Forms/InputPage";
import {TextAreaPage} from "./pages/Forms/TextAreaPage";
import capitalize from 'lodash.capitalize';
import some from 'lodash.some';
import {SelectWrapperPage} from "./pages/Forms/SelectWrapperPage";
import {CheckboxPage} from "./pages/Forms/CheckboxPage";
import {RadioPage} from "./pages/Forms/RadioPage";
import {FilePage} from "./pages/Forms/FilePage";
import {BoxPage} from "./pages/Elements/BoxPage";
import {ButtonPage} from "./pages/Elements/ButtonPage";
import {ContentPage} from "./pages/Elements/ContentPage";
import {DeletePage} from "./pages/Elements/DeletePage";
import {IconWrapperPage} from "./pages/Elements/IconWrapperPage";
import {GeneralPage} from "./pages/Forms/GeneralPage";
import {HelpPage} from "./pages/Forms/HelpPage";
import {LabelPage} from "./pages/Forms/LabelPage";
import {ImagePage} from "./pages/Elements/ImagePage";
import {NotificationPage} from "./pages/Elements/NotificationPage";
import {ProgressBarPage} from "./pages/Elements/ProgressBarPage";
import {TablePage} from "./pages/Elements/TablePage";
import {TagPage} from "./pages/Elements/TagPage";
import {TagsPage} from "./pages/Elements/TagsPage";
import {ButtonGroupPage} from "./pages/Elements/ButtonGroupPage";
import {ButtonsPage} from "./pages/Elements/ButtonsPage";
import {TitlePage} from "./pages/Elements/TitlePage";
import {ListPage} from "./pages/Elements/ListPage";
import {BreadcrumbPage} from "./pages/Components/BreadcrumbPage";
import {CardPage} from "./pages/Components/CardPage";
import {DropdownPage} from "./pages/Components/DropdownPage";
import {MenuPage} from "./pages/Components/MenuPage";
import {MessagePage} from "./pages/Components/MessagePage";
import {ModalPage} from "./pages/Components/ModalPage";
import {NavbarPage} from "./pages/Components/NavbarPage";
import {PaginationPage} from "./pages/Components/PaginationPage";
import {PanelPage} from "./pages/Components/PanelPage";
import {TabsPage} from "./pages/Components/TabsPage";
import {ExtensionsInstallation} from "./pages/Initial/ExtensionsInstallation";
import {SliderPage} from "./pages/Extensions/SliderPage";
import {TooltipPage} from "./pages/Extensions/TooltipPage";

export const CATEGORY_NAMES = {
  HOME: 'home',
  INITIAL: 'initial',
  COLUMNS: 'columns',
  LAYOUT: 'layout',
  FORM: 'form',
  ELEMENTS: 'elements',
  COMPONENTS: 'components',
  EXTENSIONS: 'extensions',
};

export const ROUTES = [
  {path: '/', exact: true, name: 'Home', category: CATEGORY_NAMES.INITIAL,  render: () => <Home/> },
  {path: '/start', exact: true, name: 'Getting Started', category: CATEGORY_NAMES.INITIAL, render: () => <GettingStarted/> },
  {path: '/extensions_installation', exact: true, name: 'Extensions', category: CATEGORY_NAMES.INITIAL, render: () => <ExtensionsInstallation/> },
  {path: '/columns', exact: true, name: 'Columns', category: CATEGORY_NAMES.COLUMNS, render: () => <ColumnsPage/> },
  {path: '/column', exact: true, name: 'Column', category: CATEGORY_NAMES.COLUMNS, render: () => <ColumnPage/> },
  {path: '/container', exact: true, name: 'Container', category: CATEGORY_NAMES.LAYOUT, render: () => <ContainerPage/> },
  {path: '/level', exact: true, name: 'Level', category: CATEGORY_NAMES.LAYOUT, render: () => <LevelPage/> },
  {path: '/media_object', exact: true, name: 'Media', category: CATEGORY_NAMES.LAYOUT, render: () => <MediaObjectPage/> },
  {path: '/hero', exact: true, name: 'Hero', category: CATEGORY_NAMES.LAYOUT, render: () => <HeroPage/> },
  {path: '/section', exact: true, name: 'Section', category: CATEGORY_NAMES.LAYOUT, render: () => <SectionPage/> },
  {path: '/footer', exact: true, name: 'Footer', category: CATEGORY_NAMES.LAYOUT, render: () => <FooterPage/> },
  {path: '/tile', exact: true, name: 'Tiles', category: CATEGORY_NAMES.LAYOUT, render: () => <TilePage/> },
  {path: '/form', exact: true, name: 'General', category: CATEGORY_NAMES.FORM, render: () => <GeneralPage/> },
  {path: '/input', exact: true, name: 'Input', category: CATEGORY_NAMES.FORM, render: () => <InputPage/> },
  {path: '/textarea', exact: true, name: 'TextArea', category: CATEGORY_NAMES.FORM, render: () => <TextAreaPage/> },
  {path: '/select_wrapper', exact: true, name: 'Select Wrapper', category: CATEGORY_NAMES.FORM, render: () => <SelectWrapperPage/> },
  {path: '/checkbox', exact: true, name: 'Checkbox', category: CATEGORY_NAMES.FORM, render: () => <CheckboxPage/> },
  {path: '/radio', exact: true, name: 'Radio', category: CATEGORY_NAMES.FORM, render: () => <RadioPage/> },
  {path: '/file', exact: true, name: 'File', category: CATEGORY_NAMES.FORM, render: () => <FilePage/> },
  {path: '/form_help', exact: true, name: 'Help', category: CATEGORY_NAMES.FORM, render: () => <HelpPage/> },
  {path: '/form_label', exact: true, name: 'Label', category: CATEGORY_NAMES.FORM, render: () => <LabelPage/> },
  {path: '/box', exact: true, name: 'Box', category: CATEGORY_NAMES.ELEMENTS, render: () => <BoxPage/> },
  {path: '/button', exact: true, name: 'Button', category: CATEGORY_NAMES.ELEMENTS, render: () => <ButtonPage/> },
  {path: '/buttons', exact: true, name: 'Buttons', category: CATEGORY_NAMES.ELEMENTS, render: () => <ButtonsPage/> },
  {path: '/button_group', exact: true, name: 'Button Group', category: CATEGORY_NAMES.ELEMENTS, render: () => <ButtonGroupPage/> },
  {path: '/content', exact: true, name: 'Content', category: CATEGORY_NAMES.ELEMENTS, render: () => <ContentPage/> },
  {path: '/delete', exact: true, name: 'Delete', category: CATEGORY_NAMES.ELEMENTS, render: () => <DeletePage/> },
  {path: '/icon_wrapper', exact: true, name: 'Icon Wrapper', category: CATEGORY_NAMES.ELEMENTS, render: () => <IconWrapperPage/> },
  {path: '/image', exact: true, name: 'Image', category: CATEGORY_NAMES.ELEMENTS, render: () => <ImagePage/> },
  {path: '/notification', exact: true, name: 'Notification', category: CATEGORY_NAMES.ELEMENTS, render: () => <NotificationPage/> },
  {path: '/progress_bar', exact: true, name: 'Progress Bar', category: CATEGORY_NAMES.ELEMENTS, render: () => <ProgressBarPage/> },
  {path: '/table', exact: true, name: 'Table', category: CATEGORY_NAMES.ELEMENTS, render: () => <TablePage/> },
  {path: '/tag', exact: true, name: 'Tag', category: CATEGORY_NAMES.ELEMENTS, render: () => <TagPage/> },
  {path: '/tags', exact: true, name: 'Tags', category: CATEGORY_NAMES.ELEMENTS, render: () => <TagsPage/> },
  {path: '/title', exact: true, name: 'Title', category: CATEGORY_NAMES.ELEMENTS, render: () => <TitlePage/> },
  {path: '/list', exact: true, name: 'List', category: CATEGORY_NAMES.ELEMENTS, render: () => <ListPage/> },
  {path: '/breadcrumb', exact: true, name: 'Breadcrumb', category: CATEGORY_NAMES.COMPONENTS, render: () => <BreadcrumbPage/> },
  {path: '/card', exact: true, name: 'Card', category: CATEGORY_NAMES.COMPONENTS, render: () => <CardPage/> },
  {path: '/dropdown', exact: true, name: 'Dropdown', category: CATEGORY_NAMES.COMPONENTS, render: () => <DropdownPage/> },
  {path: '/menu', exact: true, name: 'Menu', category: CATEGORY_NAMES.COMPONENTS, render: () => <MenuPage/> },
  {path: '/message', exact: true, name: 'Message', category: CATEGORY_NAMES.COMPONENTS, render: () => <MessagePage/> },
  {path: '/modal', exact: true, name: 'Modal', category: CATEGORY_NAMES.COMPONENTS, render: () => <ModalPage/> },
  {path: '/navbar', exact: true, name: 'Navbar', category: CATEGORY_NAMES.COMPONENTS, render: () => <NavbarPage/> },
  {path: '/pagination', exact: true, name: 'Pagination', category: CATEGORY_NAMES.COMPONENTS, render: () => <PaginationPage/> },
  {path: '/panel', exact: true, name: 'Panel', category: CATEGORY_NAMES.COMPONENTS, render: () => <PanelPage/> },
  {path: '/tabs', exact: true, name: 'Tabs', category: CATEGORY_NAMES.COMPONENTS, render: () => <TabsPage/> },
  {path: '/slider', exact: true, name: 'Slider', category: CATEGORY_NAMES.EXTENSIONS, render: () => <SliderPage/> },
  {path: '/tooltip', exact: true, name: 'Tooltip', category: CATEGORY_NAMES.EXTENSIONS, render: () => <TooltipPage/> }
];

export const CATEGORIES = Object.keys(CATEGORY_NAMES).map( categoryName => ({
  key: categoryName,
  name: capitalize(categoryName.toLocaleLowerCase()),
  routes() { return ROUTES.filter(route => route.category === CATEGORY_NAMES[this.key]); },
  hasRoutes() { return some(ROUTES,route => route.category === CATEGORY_NAMES[this.key]); }
  })
);
export const CATEGORIES_WITH_ROUTES = CATEGORIES.filter(category => category.hasRoutes());