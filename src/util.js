export const EDITOR_OPTIONS = { mode: "jsx", theme: "dracula", readOnly: true,
  width: '100%', height: '135px', fontSize: '1.125em', name: 'ace-editor-box',
  editorProps: { $blockScrolling: true }
};

export const TYPES = {
  STRING: 'String',
  NUMBER: 'Number',
  BOOLEAN: 'Boolean',
  FUNCTION: 'Function',
  COMPONENT: 'React Component'
};

export const MOBILE     = 'mobile';
export const TABLET     = 'tablet';
export const TOUCH      = 'touch';
export const DESKTOP    = 'desktop';
export const WIDESCREEN = 'widescreen';
export const FULLHD     = 'fullhd';
export const DEVICES    = [MOBILE, TABLET, TOUCH, DESKTOP, WIDESCREEN, FULLHD];
export const COLOR_TYPES = ['primary', 'link', 'info', 'success', 'warning', 'danger', 'black','dark','light','white'];
export const SIZES = ['small', 'normal', 'medium', 'large'];
export const SIZE_NUMBERS = ['1','2','3','4','5','6'];
export const BUTTON_POSITIONS = ['centered', 'right'];
export const ICON_POSITIONS = ['left', 'right'];
export const ALIGNMENTS = ['centered', 'right'];
export const HIERARCHIES = ['ancestor', 'parent', 'child'];
export const STATES = ['hovered', 'focused'];
export const SEPARATORS = ['arrow','bullet', 'dot', 'succeeds'];
export const POSITIONS = ['top', 'bottom', 'left', 'right'];
export const ORIENTATIONS = ['vertical', 'horizontal'];
export const buildPropsRow = (name, type, defaultValue, description, {visible} = {visible: true}) => ({name, type, defaultValue, description, visible});
export const booleanClassMessage = (propName, {prefix = 'is'} = {prefix: 'is'}) => `Used to determine whether to add <code>${prefix}-${propName}</code> class or not.`;
export const bulmaPropMessage  = (component = 'component', property = 'property') => `Sets the ${component} <b>${property}</b>.`;
export const colorClassMessage = (component = 'component') => bulmaPropMessage(component, 'color');
export const sizeClassMessage  = (component = 'component') => bulmaPropMessage(component, 'size');
export const stateClassMessage = (component = 'component') => bulmaPropMessage(component, 'state');