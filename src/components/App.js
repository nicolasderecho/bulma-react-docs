import React from 'react';
import {Sidebar} from "./shared/Sidebar";
import {Main} from "./Main";
import { withRouter } from "react-router-dom"
import 'brace/theme/dracula';
import 'brace/mode/jsx';
import {MainNavbar} from "./shared/MainNavbar";

const elementSupportsSmoothScroll = (element) => !!element.style && element.style.hasOwnProperty('scrollBehavior');
const scrollToTop = () => {
  const mainElement = document.querySelector('#main') || document.querySelector('body');
  if(elementSupportsSmoothScroll(mainElement)){
    mainElement.scrollTo({top: 0, behavior: 'smooth'});
  }
  else {
    mainElement.scrollTo(0,0);
  }
};

class AppComponent extends React.Component {

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.routeHasChanged(prevProps)) {
      this.onRouteChange();
    }
  }

  routeHasChanged(prevProps) {
    return this.props.location !== prevProps.location;
  }

  onRouteChange(){
    scrollToTop();
  }

  render() {
    return <div className="application fill-column">
      <MainNavbar/>
      <div className={'application-content'}>
        <Sidebar className={'is-hidden-touch'}/>
        <Main/>
      </div>
    </div>;
  }
}

const App = withRouter(AppComponent);
export default App;
