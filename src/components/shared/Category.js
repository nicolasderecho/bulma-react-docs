import React, {useState} from 'react';
import classNames from 'classnames';
import {IconWrapper} from "tenpines-bulma-react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons'

export const Category = ({className, name, items = [], startOpened = false, renderItem = (item) => item, ...props}) => {
  const [isOpened,setIsOpened] = useState(startOpened);
  const toggleOpened = () => setIsOpened(!isOpened);
  const classes = classNames(className, 'category-container', {'is-opened': isOpened});
  const icon = isOpened ? faAngleUp : faAngleDown;
  return <div className={classes} {...props}>
      <header className={'category-header'} onClick={toggleOpened}>
        <span>{name}</span>
        <IconWrapper>
          <FontAwesomeIcon icon={icon} />
        </IconWrapper>
      </header>
      <ul className={'category-items'}>
        { items.map((item, index) => <li key={item.key || index} className={'category-item'}>{renderItem(item)}</li>) }
      </ul>
    </div>;
};