import React from 'react';
import {Table} from "tenpines-bulma-react";
import {buildPropsRow, TYPES} from "../../util";

const isVisibleRow = (row) => !!row.visible;
const renderRow = (row) => <Table.Row key={row.name || Math.random()}>
  <Table.Cell>{row.name}</Table.Cell>
  <Table.Cell>{row.type}</Table.Cell>
  <Table.Cell dangerouslySetInnerHTML={{__html: row.defaultValue}} />
  <Table.Cell dangerouslySetInnerHTML={{__html: row.description}} />
</Table.Row>;

const PropsTable = ({bordered = true, includeClassProps = true, includeAsProp = true, defaultAsValue = 'div', rows = [] }) => {
  const allDefaultRows = [
    buildPropsRow('className', TYPES.STRING, '', 'Classes to append to the element.', {visible: includeClassProps}),
    buildPropsRow('as', TYPES.STRING, `<code>${defaultAsValue}</code>`, 'Sets the HTML element used as main element.', {visible: includeAsProp}),
  ];
  const defaultRows = allDefaultRows.filter(isVisibleRow);
  const rowsToDisplay = defaultRows.concat(rows);
  return <Table.Container>
    <Table bordered={bordered}>
      <Table.Head>
        <Table.Row>
          <Table.CellHeader>Name</Table.CellHeader>
          <Table.CellHeader>Type</Table.CellHeader>
          <Table.CellHeader>Default</Table.CellHeader>
          <Table.CellHeader>Description</Table.CellHeader>
        </Table.Row>
      </Table.Head>
      <Table.Body>
        { rowsToDisplay.map(row => renderRow(row)) }
      </Table.Body>
    </Table>
  </Table.Container>
};

export {PropsTable};