import React, {useState, useEffect, useRef} from 'react';
import classNames from 'classnames';
import {Image, Navbar} from "tenpines-bulma-react";
import logo from '../../assets/10Pines-pino.svg';
import {Sidebar} from "./Sidebar";

const MainNavbarComponent = () => {
  const mountedRef = useRef(false);
  const [openSidebar, updateOpenSidebar] = useState(false);
  const toggleSidebar = () => updateOpenSidebar(!openSidebar);
  const sidebarClasses = classNames('navbar-sidebar', 'animated', 'fast', {slideInLeft: openSidebar, slideOutLeft: mountedRef.current && !openSidebar, 'is-hidden': !mountedRef.current && !openSidebar});
  useEffect(()=> { mountedRef.current = true }, []);
  return <React.Fragment>
    <Navbar className={'main-navbar is-hidden-desktop'}>
      <Navbar.Brand>
        <Image src={logo} is={'48x48'}/>
        <Navbar.Burger onClick={toggleSidebar} />
      </Navbar.Brand>
    </Navbar>
    <div className={sidebarClasses}>
      <Sidebar className={'is-hidden-desktop'} onSelectRoute={toggleSidebar}/>
    </div>
  </React.Fragment>
};

const MainNavbar = MainNavbarComponent;

export { MainNavbar };