import React from 'react';
import classNames from 'classnames';
import {Image} from "tenpines-bulma-react";
import logo from '../../assets/10Pines-pino-blanco.svg';
import {withRouter, NavLink} from 'react-router-dom';
import {CATEGORIES_WITH_ROUTES} from "../../routes";
import {Category} from "./Category";
import some from "lodash.some";

const Link = ({to, children, exact = true, onClick = () => {}, ...props }) => <NavLink to={to} exact={exact} onClick={onClick} className={'link'} activeClassName={'active'} {...props}>{children}</NavLink>;
const isInAnyCategoryRoute = (routes, pathname) => some(routes, route => route.path === pathname);

const SidebarComponent = ({className, onSelectRoute = () => {}, location: { pathname } }) => {
    const renderItem = item => <Link to={item.path} key={item.path} onClick={onSelectRoute} >{item.name}</Link>;
    const classes = classNames('sidebar', className);
    return <div className={classes}>
        <div className={'sidebar-content'}>
            <div className={'logo-container'}>
                <Image is={'48x48'} src={logo} className={'logo'}/>
                <span>Bulma React</span>
            </div>
            <div className={'navigation-links'}>
                { CATEGORIES_WITH_ROUTES.map( category => <Category key={category.key} name={category.name} startOpened={isInAnyCategoryRoute(category.routes(), pathname)} items={category.routes()} renderItem={renderItem} /> ) }
            </div>
        </div>
    </div>
};

const Sidebar = withRouter(SidebarComponent);
export { Sidebar };