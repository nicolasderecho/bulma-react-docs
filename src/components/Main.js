import React from 'react';
import { Route, Switch } from "react-router-dom"
import {NotFound} from "../pages/NotFound";
import {Container} from "tenpines-bulma-react";
import {ROUTES} from "../routes";

export const Main = () => {
    return <div id={'main'} className={'main'}>
        <div className={'main-content'}>
            <Container className={'root-container'} fluid>
                <Switch>
                    { ROUTES.map( (route) =>
                        <Route path={route.path} key={route.path} exact={route.exact} render={route.render} />)
                    }
                    <Route render={() => <NotFound/>} />
                </Switch>
            </Container>
        </div>
    </div>
};