import React from 'react';
import {Subtitle, Title, Columns, Column} from "tenpines-bulma-react";
import {booleanClassMessage, buildPropsRow, TYPES, DEVICES, bulmaPropMessage} from "../../util";
import {PropsTable} from "../../components/shared/PropsTable";

const columnSizeProp = buildPropsRow('columnSize', TYPES.STRING, '', bulmaPropMessage('column', 'size'));
const deviceColumnSizeProps = DEVICES.map(device => buildPropsRow(`${device}ColumnSize`, TYPES.STRING, '', `Sets the column size in <b>${device}</b> devices.`));
const offsetProp = buildPropsRow('offset', TYPES.STRING, '', bulmaPropMessage('offset', 'size'));
const deviceOffsetProps = DEVICES.map(device => buildPropsRow(`${device}Offset`, TYPES.STRING, '', `Sets the offset size in <b>${device}</b> devices.`));
const narrowProp =  buildPropsRow('narrow', TYPES.BOOLEAN, '', booleanClassMessage('narrow'));
const deviceNarrowProps = DEVICES.map(device => buildPropsRow(`${device}Narrow`, TYPES.BOOLEAN, '', `Used to determine whether to enable <b>narrow</b> prop or not on <b>${device}</b> devices.`));
const componentProps = [].concat(columnSizeProp, deviceColumnSizeProps, offsetProp, deviceOffsetProps, narrowProp, deviceNarrowProps);

export const ColumnPage = () => {
    return <React.Fragment>
        <Title>Column</Title>
        <br />
        <Subtitle>Props</Subtitle>
        <Columns>
            <Column>
                <PropsTable rows={componentProps} />
            </Column>
        </Columns>
    </React.Fragment>;
};