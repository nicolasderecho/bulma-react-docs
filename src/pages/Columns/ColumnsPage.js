import React from 'react';
import {Subtitle, Title, Columns, Column, Container} from "tenpines-bulma-react";
import AceEditor from 'react-ace';
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, TYPES} from "../../util";
import {PropsTable} from "../../components/shared/PropsTable";

const example1 = `import React from 'react';
import {Columns, Column} from 'tenpines-bulma-react';

<Columns>
    <Column><p>1</p></Column>
    <Column><p>2</p></Column>
</Columns>`;

const example2 = `import React from 'react';
import {Columns, Column} from 'tenpines-bulma-react';

<Columns vcentered>
    <Column>
      <p>First column</p>
    </Column>
    <Column>
      <p>Second column with more content. This is so you can see the vertical alignment.</p>
    </Column>
</Columns>`;

const example3 = `import React from 'react';
import {Columns, Column} from 'tenpines-bulma-react';

<Columns gap={2} gapDesktop={5} gapMobile={1} gapTablet={2} gapFullhd={8}>
    <Column><p>First column</p></Column>
    <Column><p>Second column</p></Column>
</Columns>`;

const example4 = `import React from 'react';
import {Columns, Column} from 'tenpines-bulma-react';

<Columns multiline>
    <Column columnSize={'one-quarter'}><p>One Quarter</p></Column>
    <Column columnSize={'one-quarter'}><p>One Quarter</p></Column>
    <Column columnSize={'one-quarter'}><p>One Quarter</p></Column>
    <Column columnSize={'one-quarter'}><p>One Quarter</p></Column>
    <Column columnSize={'half'}><p>Is Half</p></Column>
    <Column columnSize={'half'}><p>Is Half</p></Column>
</Columns>`;

const example5 = `import React from 'react';
import {Columns, Column} from 'tenpines-bulma-react';

<Columns from={'mobile'}>
    <Column columnSize={'one-quarter'}><p>Mobile</p></Column>
    <Column columnSize={'one-quarter'}><p>Mobile</p></Column>
    <Column columnSize={'one-quarter'}><p>Mobile</p></Column>
    <Column columnSize={'one-quarter'}><p>Mobile</p></Column>
</Columns>`;

const gapNumberMessage = (device) => `Sets the gap number for <b>${device}</b> devices.`;
const componentProps = [
    buildPropsRow('gap', TYPES.NUMBER, '', 'Sets the gap number.'),
    buildPropsRow('gapless', TYPES.BOOLEAN, '', booleanClassMessage('gapless')),
    buildPropsRow('multiline', TYPES.BOOLEAN, '', booleanClassMessage('multiline')),
    buildPropsRow('centered', TYPES.BOOLEAN, '', booleanClassMessage('centered')),
    buildPropsRow('vcentered', TYPES.BOOLEAN, '', booleanClassMessage('vcentered')),
    buildPropsRow('gapMobile', TYPES.NUMBER, '', gapNumberMessage('mobile')),
    buildPropsRow('gapTablet', TYPES.NUMBER, '', gapNumberMessage('tablet')),
    buildPropsRow('gapTouch', TYPES.NUMBER, '', gapNumberMessage('touch')),
    buildPropsRow('gapDesktop', TYPES.NUMBER, '', gapNumberMessage('desktop')),
    buildPropsRow('gapWidescreen', TYPES.NUMBER, '', gapNumberMessage('widescreen')),
    buildPropsRow('gapFullhd', TYPES.NUMBER, '', gapNumberMessage('desktop')),
    buildPropsRow('from', TYPES.STRING, '', 'Sets the minimum device since the Columns are activated.'),
];

export const ColumnsPage = () => {
    return <React.Fragment>
        <Title>Columns</Title>
        <br />
        <Subtitle>Props</Subtitle>
        <Columns>
            <Column>
                <PropsTable rows={componentProps} />
            </Column>
        </Columns>
        <Subtitle>Examples</Subtitle>
        <Subtitle>Basic</Subtitle>
        <Columns>
            <Column>
                <AceEditor {...EDITOR_OPTIONS} name={'basic-example'} height={'135px'} value={example1} />
            </Column>
        </Columns>
        <Columns>
            <Column><p className="bd-notification is-primary">1</p></Column>
            <Column><p className="bd-notification is-primary">2</p></Column>
        </Columns>
        <Subtitle>Vertically centered columns</Subtitle>
        <Columns>
            <Column>
                <AceEditor {...EDITOR_OPTIONS} name={'vcentered columns'} value={example2} height={'200px'} />
            </Column>
        </Columns>
        <Columns vcentered>
            <Column><p className="bd-notification is-primary horizontal-padding">First column</p></Column>
            <Column><p className="bd-notification is-primary horizontal-padding">Second column with more content. This is so you can
                see the vertical alignment on this awesome page.</p></Column>
        </Columns>
        <br/>
        <Subtitle>Setting Gaps for the specified device</Subtitle>
        <Columns>
            <Column>
                <AceEditor {...EDITOR_OPTIONS} name={'gaps-columns'} value={example3} height={'150px'} />
            </Column>
        </Columns>
        <Container fluid>
            <Columns gap={2} gapDesktop={5} gapMobile={1} gapTablet={2} gapFullhd={8}>
                <Column><p className="bd-notification is-primary">First column</p></Column>
                <Column><p className="bd-notification is-primary">Second column</p></Column>
            </Columns>
        </Container>
        <br/>
        <Subtitle>Multiline Columns</Subtitle>
        <Columns>
            <Column>
                <AceEditor {...EDITOR_OPTIONS} name={'multiline-columns'} value={example4} height={'200px'} />
            </Column>
        </Columns>
        <Columns multiline>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">One Quarter</p></Column>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">One Quarter</p></Column>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">One Quarter</p></Column>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">One Quarter</p></Column>
            <Column columnSize={'half'}><p className="bd-notification is-primary horizontal-padding">Is Half</p></Column>
            <Column columnSize={'half'}><p className="bd-notification is-primary horizontal-padding">Is Half</p></Column>
        </Columns>
        <br/>
        <Subtitle>Mobile Columns</Subtitle>
        <Columns>
            <Column>
                <p>By default, columns are only activated from tablet onwards. This means columns are stacked on top of each other on mobile.
                    If you want columns to work on mobile too you can use <code>from</code> prop to specify the starting device</p>
            </Column>
        </Columns>
        <Columns>
            <Column>
                <AceEditor {...EDITOR_OPTIONS} name={'mobile-columns'} value={example5} height={'170px'} />
            </Column>
        </Columns>
        <Columns from={'mobile'}>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">Mobile</p></Column>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">Mobile</p></Column>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">Mobile</p></Column>
            <Column columnSize={'one-quarter'}><p className="bd-notification is-primary horizontal-padding">Mobile</p></Column>
        </Columns>`
    </React.Fragment>;
};