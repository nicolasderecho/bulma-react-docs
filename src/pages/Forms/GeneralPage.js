import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const controlRows = [
  buildPropsRow('expanded', TYPES.BOOLEAN, '', booleanClassMessage('expanded')),
  buildPropsRow('loading', TYPES.BOOLEAN, '', booleanClassMessage('loading')),
  buildPropsRow('hasIconsLeft', TYPES.BOOLEAN, '', booleanClassMessage('icons-left', {prefix: 'has'})),
  buildPropsRow('hasIconsRight', TYPES.BOOLEAN, '', booleanClassMessage('icons-right', {prefix: 'has'}))
];

const fieldRows = [
  buildPropsRow('grouped', TYPES.BOOLEAN, '', booleanClassMessage('grouped')),
  buildPropsRow('groupedCentered', TYPES.BOOLEAN, '', booleanClassMessage('grouped-centered')),
  buildPropsRow('groupedRight', TYPES.BOOLEAN, '', booleanClassMessage('grouped-right')),
  buildPropsRow('groupedMultiline', TYPES.BOOLEAN, '', booleanClassMessage('grouped-multiline')),
  buildPropsRow('horizontal', TYPES.BOOLEAN, '', booleanClassMessage('horizontal')),
  buildPropsRow('addons', TYPES.BOOLEAN, '', booleanClassMessage('right', {prefix: 'has'})),
  buildPropsRow('alignment', TYPES.STRING, '', 'Sets the addons alignment')
];

const fieldLabelRows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('field label'))
];

export const GeneralPage = () => {
  return <React.Fragment>
    <Title>General</Title>
    <Subtitle>All generic form controls, designed for consistency.</Subtitle>
    <br/>

    <Title>Control</Title>
    <Subtitle>A simple div with class <b>control</b>.</Subtitle>
    <PropsTable includeAsProp={false} rows={controlRows}/>

    <Title>Field</Title>
    <Subtitle>A simple div with class <b>field</b>.</Subtitle>
    <PropsTable includeAsProp={false} rows={fieldRows}/>

    <Title>FieldLabel</Title>
    <Subtitle>A div used in horizontal forms as <b>label</b> container.</Subtitle>
    <PropsTable rows={fieldLabelRows}/>

    <Title>FieldBody</Title>
    <Subtitle>A div used in horizontal forms as <b>input/select/textarea</b> container.</Subtitle>
    <PropsTable rows={fieldLabelRows}/>

    <Title>DisabledFieldset</Title>
    <Subtitle>This component simply renders a <code>fieldset</code> with the HTML attribute <b>disabled</b>.</Subtitle>

  </React.Fragment>;
};