import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import { booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, stateClassMessage, TYPES } from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('select wrapper')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('select wrapper')),
  buildPropsRow('state', TYPES.STRING, '', stateClassMessage('select wrapper')),
  buildPropsRow('fullwidth', TYPES.BOOLEAN, '', booleanClassMessage('fullwidth')),
  buildPropsRow('multiple', TYPES.BOOLEAN, '', booleanClassMessage('multiple'))
];

export const SelectWrapperPage = () => {
  return <React.Fragment>
    <Title>SelectWrapper</Title>
    <Subtitle>A container div for the browser built-in select dropdown, styled accordingly.</Subtitle>
    <PropsTable rows={rows} />
  </React.Fragment>;
};