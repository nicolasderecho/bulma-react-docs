import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, TYPES} from "../../util";

const rows = [
  buildPropsRow('wrapperClassname', TYPES.STRING, '', 'classes to add to the label that wraps the input')
];
export const CheckboxPage = () => {
  return <React.Fragment>
    <Title>Checkbox</Title>
    <Subtitle>The 2-state checkbox in its native format.</Subtitle>
    <PropsTable rows={rows} includeAsProp={false} />
  </React.Fragment>;
};