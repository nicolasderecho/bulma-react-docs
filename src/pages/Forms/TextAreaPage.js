import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, stateClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('textarea')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('textarea')),
  buildPropsRow('state', TYPES.STRING, '', stateClassMessage('textarea')),
  buildPropsRow('hasFixedSize', TYPES.STRING, '', booleanClassMessage('fixed-size', {prefix: 'has'})),
];

export const TextAreaPage = () => {
  return <React.Fragment>
    <Title>TextArea</Title>
    <Subtitle>The multiline textarea and its variations.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
  </React.Fragment>;
};