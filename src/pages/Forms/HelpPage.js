import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import React from "react";
import {buildPropsRow, colorClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('help')),
];

export const HelpPage = () => {
  return <React.Fragment>
    <Title>Help</Title>
    <Subtitle>an optional help text rendered as a <b>p</b> element.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
  </React.Fragment>;
};
