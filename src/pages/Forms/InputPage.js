import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import { booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, stateClassMessage, TYPES } from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('input')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('input')),
  buildPropsRow('state', TYPES.STRING, '', stateClassMessage('input')),
  buildPropsRow('expanded', TYPES.BOOLEAN, '', booleanClassMessage('expanded')),
  buildPropsRow('rounded', TYPES.BOOLEAN, '', booleanClassMessage('rounded')),
  buildPropsRow('expanded', TYPES.BOOLEAN, '', booleanClassMessage('expanded')),
];

export const InputPage = () => {
    return <React.Fragment>
        <Title>Input</Title>
        <Subtitle>The text input and its variations.</Subtitle>
        <PropsTable rows={rows} includeAsProp={false} />
    </React.Fragment>;
};