import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, TYPES } from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('file')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('file')),
  buildPropsRow('boxed', TYPES.BOOLEAN, '', booleanClassMessage('boxed')),
  buildPropsRow('fullwidth', TYPES.BOOLEAN, '', booleanClassMessage('fullwidth')),
  buildPropsRow('hasName', TYPES.BOOLEAN, '', booleanClassMessage('name', {prefix: 'has'})),
  buildPropsRow('alignment', TYPES.STRING, '', 'Sets file alignment')
];

export const FilePage = () => {
  return <React.Fragment>
    <Title>File</Title>
    <Subtitle>A custom file upload input.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
  </React.Fragment>;
};