import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, TYPES} from "../../util";

const rows = [
  buildPropsRow('wrapperClassname', TYPES.STRING, '', 'classes to add to the label that wraps the input')
];

export const RadioPage = () => {
  return <React.Fragment>
    <Title>Radio</Title>
    <Subtitle>The mutually exclusive radio buttons in their native format.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
  </React.Fragment>;
};