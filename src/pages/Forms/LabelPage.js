import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('label')),
];

export const LabelPage = () => {
  return <React.Fragment>
    <Title>Label</Title>
    <Subtitle>A label component.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
  </React.Fragment>;
};