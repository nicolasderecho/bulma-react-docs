import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('content')),
];

export const ContentPage = () => {
  return <React.Fragment>
    <Title>Content</Title>
    <Subtitle>A single class to handle WYSIWYG generated content, where only HTML tags are available.</Subtitle>
    <PropsTable rows={rows} />
  </React.Fragment>;
};