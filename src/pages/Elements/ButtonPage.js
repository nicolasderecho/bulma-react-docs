import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import { booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, stateClassMessage, TYPES} from "../../util";

const rows=[
  buildPropsRow('type', TYPES.STRING, '<code>submit</code>','Sets the input type when using an input element'),
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('button')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('button')),
  buildPropsRow('state', TYPES.STRING, '', stateClassMessage('button')),
  buildPropsRow('outlined', TYPES.BOOLEAN, '', booleanClassMessage('outlined')),
  buildPropsRow('inverted', TYPES.BOOLEAN, '', booleanClassMessage('inverted')),
  buildPropsRow('link', TYPES.BOOLEAN, '', booleanClassMessage('link')),
  buildPropsRow('loading', TYPES.BOOLEAN, '', booleanClassMessage('loading')),
  buildPropsRow('rounded', TYPES.BOOLEAN, '', booleanClassMessage('rounded')),
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active')),
  buildPropsRow('text', TYPES.BOOLEAN, '', booleanClassMessage('text')),
];
export const ButtonPage = () => {
  return <React.Fragment>
    <Title>Button</Title>
    <Subtitle>The classic button, in different colors, sizes, and states.</Subtitle>
    <PropsTable rows={rows} defaultAsValue={'button'}/>
  </React.Fragment>;
};