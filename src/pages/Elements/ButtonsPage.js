import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";

const example1 = `import React from 'react';
import {Buttons, Button} from 'tenpines-bulma-react';

<Buttons>
  <Button>One</Button>
  <Button>Two</Button>
  <Button>Three</Button>
</Buttons>`;

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('buttons')),
  buildPropsRow('position', TYPES.STRING, '', 'Sets the button position'),
  buildPropsRow('addons', TYPES.BOOLEAN, '', booleanClassMessage('addons', {prefix: 'has'}))
];
export const ButtonsPage = () => {
  return <React.Fragment>
    <Title>Buttons</Title>
    <Subtitle>A container for a list of buttons.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'buttons-example'} height={'175px'} value={example1} />
      </Column>
    </Columns>
  </React.Fragment>;
};