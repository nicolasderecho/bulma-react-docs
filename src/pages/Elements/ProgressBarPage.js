import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('progress bar')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('progress bar')),
  buildPropsRow('indeterminated', TYPES.BOOLEAN, '', booleanClassMessage('indeterminated')),
  buildPropsRow('value', TYPES.NUMBER, '', 'Sets the progress bar value'),
  buildPropsRow('max', TYPES.NUMBER, '', 'Sets the maximum value')
];

export const ProgressBarPage = () => {
  return <React.Fragment>
    <Title>ProgressBar</Title>
    <Subtitle>Native HTML progress bars.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
  </React.Fragment>;
};