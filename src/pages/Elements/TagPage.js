import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('tag')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('tag')),
  buildPropsRow('rounded', TYPES.BOOLEAN, '', booleanClassMessage('rounded')),
  buildPropsRow('delete', TYPES.BOOLEAN, '', booleanClassMessage('delete')),
  buildPropsRow('grouped', TYPES.BOOLEAN, '', booleanClassMessage('grouped')),
  buildPropsRow('hasAddons', TYPES.BOOLEAN, '', booleanClassMessage('addons', {prefix: 'has'})),
];

export const TagPage = () => {
  return <React.Fragment>
    <Title>Tag</Title>
    <Subtitle>Small tag labels to insert anywhere.</Subtitle>
    <PropsTable defaultAsValue={'span'} rows={rows} />
  </React.Fragment>;
};