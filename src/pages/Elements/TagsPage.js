import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('tags')),
  buildPropsRow('addons', TYPES.BOOLEAN, '', booleanClassMessage('addons', {prefix: 'has'})),
];

export const TagsPage = () => {
  return <React.Fragment>
    <Title>A div container to group tags</Title>
    <Subtitle>Small tag labels to insert anywhere.</Subtitle>
    <PropsTable defaultAsValue={'div'} rows={rows} />
  </React.Fragment>;
};