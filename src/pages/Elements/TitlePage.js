import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, TYPES} from "../../util";

const rows = [
  buildPropsRow('sizeNumber', TYPES.NUMBER, '', 'Sets the title size number'),
  buildPropsRow('spaced', TYPES.BOOLEAN, '', booleanClassMessage('spaced'))
];

export const TitlePage = () => {
  return <React.Fragment>
    <Title>Title and Subtitle</Title>
    <Subtitle>Simple headings to add depth to your page.</Subtitle>
    <br />
    <Title>Title</Title>
    <PropsTable defaultAsValue={'h1'} rows={rows} />
    <br />
    <Title>Subtitle</Title>
    <PropsTable defaultAsValue={'h1'} rows={rows} />
  </React.Fragment>;
};