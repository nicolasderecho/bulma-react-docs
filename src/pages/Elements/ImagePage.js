import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, TYPES} from "../../util";

const rows = [
  buildPropsRow('wrapper', TYPES.STRING, '', 'Sets the image wrapper. default <code>figure</code>'),
  buildPropsRow('is', TYPES.STRING, '', 'Sets the image size'),
  buildPropsRow('src', TYPES.STRING, '', 'Sets the <code>img</code> src attribute'),
  buildPropsRow('alt', TYPES.STRING, '', 'Sets the <code>img</code> alt attribute'),
  buildPropsRow('wrapperClasses', TYPES.STRING, '', 'Classes to append to the <code>figure</code> wrapper element.'),
  buildPropsRow('rounded', TYPES.BOOLEAN, '', booleanClassMessage('rounded'))
];

export const ImagePage = () => {
  return <React.Fragment>
    <Title>Image</Title>
    <Subtitle>A container for responsive images.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows}/>
  </React.Fragment>;
};