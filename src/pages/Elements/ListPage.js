import React from 'react';
import {Subtitle, Title, Help, Notification, Columns, Column} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, EDITOR_OPTIONS, TYPES} from "../../util";
import AceEditor from "react-ace";

const rows = [
  buildPropsRow('marker', TYPES.STRING, '', 'Sets the list marker.'),
  buildPropsRow('ordered', TYPES.BOOLEAN, '', 'Used to determine whether to use an ordered list <code>ol</code> or not'),
  buildPropsRow('unordered', TYPES.BOOLEAN, '', 'Used to determine whether to use an unordered list <code>ul</code> or not'),
  buildPropsRow('description', TYPES.BOOLEAN, '', 'Used to determine whether to use a description list <code>dl</code> or not')
];

const example1 = `import React from 'react';
import { OrderedList, UnorderedList, DescriptionList, List } from 'tenpines-bulma-react';

//this
<OrderedList><li>1</li></OrderedList>
//is the same as
<List ordered><li>1</li></List>
//or
<List as="ol"><li>1</li></List>

//this
<UnorderedList><li>1</li></UnorderedList>
//is the same as
<List unordered><li>1</li></List>
//or
<List as="ul"><li>1</li></List>

//this
<DescriptionList><li>1</li></DescriptionList>
//is the same as
<List description><li>1</li></List>
//or
<List as="dl"><li>1</li></List>`;

const example2 = `import React from 'react';
import { UnorderedList, ListItem } from 'tenpines-bulma-react';

//this
<UnorderedList>
    <ListItem>1</ListItem>
</UnorderedList>
//is the same as
<UnorderedList>
    <li>1</li>
</UnorderedList>`;

export const ListPage = () => {
  return <React.Fragment>
    <Title>List</Title>
    <Subtitle>A React component to write the common HTML lists like ul/ol/dl.</Subtitle>
    <Help>the <b>as</b> prop is not necessary if you use any of the boolean props <b>ordered</b> <b>unordered</b> or <b>description</b> to set the list type.</Help>
    <PropsTable defaultAsValue={'ul'} rows={rows} />
    <Notification color={'primary'}>As a shortcut to define your list type, you can also use the React Components <b>OrderedList</b> <b>UnorderedList</b> and <b>DescriptionList</b>.</Notification>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'lists-example'} height={'450px'} value={example1} />
      </Column>
    </Columns>
    <Title>ListItem</Title>
    <Subtitle>A React component to write a HTML list item.</Subtitle>
    <PropsTable includeAsProp={false} />
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'list-item-example'} height={'220px'} value={example2} />
      </Column>
    </Columns>
  </React.Fragment>;
};