import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, TYPES} from "../../util";

const tableRows = [
  buildPropsRow('hoverable', TYPES.BOOLEAN, '', booleanClassMessage('hoverable')),
  buildPropsRow('bordered', TYPES.BOOLEAN, '', booleanClassMessage('bordered')),
  buildPropsRow('striped', TYPES.BOOLEAN, '', booleanClassMessage('striped')),
  buildPropsRow('narrow', TYPES.BOOLEAN, '', booleanClassMessage('narrow')),
  buildPropsRow('fullwidth', TYPES.BOOLEAN, '', booleanClassMessage('fullwidth'))
];

const trRows = [
  buildPropsRow('selected', TYPES.BOOLEAN, '', booleanClassMessage('selected'))
];

const htmlElementSubtitle = (element) => `a React component to render a <code>${element}</code> HTML element.`;

export const TablePage = () => {
  return <React.Fragment>
    <Title>Table</Title>
    <Subtitle>The inevitable HTML table, with special case cells.</Subtitle>
    <PropsTable includeAsProp={false} rows={tableRows} />

    <Title>TableContainer</Title>
    <Subtitle>You can create a <b>scrollable table</b> by wrapping a <code>table</code> on a <b>TableContainer</b>.</Subtitle>
    <PropsTable includeAsProp={false} />

    <Title>TableRow</Title>
    <Subtitle dangerouslySetInnerHTML={{__html: htmlElementSubtitle('tr') }} />
    <PropsTable includeAsProp={false} rows={trRows} />

    <Title>TableBody</Title>
    <Subtitle dangerouslySetInnerHTML={{__html: htmlElementSubtitle('tbody') }} />
    <PropsTable includeAsProp={false} />

    <Title>TableCell</Title>
    <Subtitle dangerouslySetInnerHTML={{__html: htmlElementSubtitle('td') }} />
    <PropsTable includeAsProp={false} />

    <Title>TableCellHeader</Title>
    <Subtitle dangerouslySetInnerHTML={{__html: htmlElementSubtitle('th') }} />
    <PropsTable includeAsProp={false} />

    <Title>TableFooter</Title>
    <Subtitle dangerouslySetInnerHTML={{__html: htmlElementSubtitle('tfoot')}} />
    <PropsTable includeAsProp={false} />

    <Title>TableHead</Title>
    <Subtitle dangerouslySetInnerHTML={{__html: htmlElementSubtitle('thead') }}/>
    <PropsTable includeAsProp={false} />
  </React.Fragment>;
};