import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('icon wrapper')),
  buildPropsRow('hasText', TYPES.STRING, '', 'Sets the text color'),
  buildPropsRow('position', TYPES.STRING, '', 'Sets the icon position'),
];

export const IconWrapperPage = () => {
  return <React.Fragment>
    <Title>IconWrapper</Title>
    <Subtitle>A wrapper for your icons. Bulma is compatible with all icon font libraries: Font Awesome 5, Font Awesome 4, Material Design Icons, Open Iconic, Ionicons etc.</Subtitle>
    <PropsTable defaultAsValue={'span'} rows={rows}/>
  </React.Fragment>;
};