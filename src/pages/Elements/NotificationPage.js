import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, colorClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('notification')),
];

export const NotificationPage = () => {
  return <React.Fragment>
    <Title>Notification</Title>
    <Subtitle>Bold notification blocks, to alert your users of something.</Subtitle>
    <PropsTable defaultAsValue={'div'} rows={rows} />
  </React.Fragment>;
};