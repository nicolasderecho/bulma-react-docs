import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {EDITOR_OPTIONS} from "../../util";

const example1 = `import React from 'react';
import {ButtonGroup, Field, Button} from 'tenpines-bulma-react';

//This component
<ButtonGroup>
    <Button>Text</Button>
</ButtonGroup>

//Is the same as
<Field grouped={true}>
    <Button>Text</Button>
</Field>`;

export const ButtonGroupPage = () => {
  return <React.Fragment>
    <Title>ButtonGroup</Title>
    <Subtitle>A simple shortcut to add a <b>grouped Field</b> component.</Subtitle>
    <PropsTable includeAsProp={false} />
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'button-group-example'} height={'225px'} value={example1} />
      </Column>
    </Columns>
  </React.Fragment>;
};