import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('delete')),
];

export const DeletePage = () => {
  return <React.Fragment>
    <Title>Delete</Title>
    <Subtitle>A versatile delete cross.</Subtitle>
    <PropsTable rows={rows} />
  </React.Fragment>;
};