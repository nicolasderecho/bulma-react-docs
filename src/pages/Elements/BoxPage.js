import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";

export const BoxPage = () => {
  return <React.Fragment>
    <Title>Box</Title>
    <Subtitle>A white box to contain other elements.</Subtitle>
    <PropsTable includeAsProp={false} />
  </React.Fragment>;
};