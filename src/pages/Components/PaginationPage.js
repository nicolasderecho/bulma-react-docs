import React from 'react';
import {Title, Subtitle, Pagination, Columns, Column} from "tenpines-bulma-react";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";

const example = `import React from 'react';
import {Pagination} from 'tenpines-bulma-react'

export const PaginationExample = () => {
return <Pagination rounded alignment={'centered'}>
  <Pagination.Previous>Previous</Pagination.Previous>
  <Pagination.Next>Next</Pagination.Next>
  <Pagination.List>
    <Pagination.Link>1</Pagination.Link>
    <Pagination.Ellipsis />
    <Pagination.Link>2</Pagination.Link>
  </Pagination.List>
</Pagination>
}
`;

const rows = [
  buildPropsRow('alignment', TYPES.STRING, '', 'Sets the pagination alignment.'),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('pagination')),
  buildPropsRow('rounded', TYPES.BOOLEAN, '', booleanClassMessage('rounded'))
];

const linkRows = [
  buildPropsRow('current', TYPES.BOOLEAN, '', booleanClassMessage('current'))
];

const ellipsisRows = [
  buildPropsRow('text', TYPES.STRING, '', 'the text to be used as range separator. Default to <code>...</code>')
];

export const PaginationPage = () => {
  return <React.Fragment>
    <Title>Pagination</Title>
    <Subtitle>A responsive, usable, and flexible pagination.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
    <br />
    <Title>PaginationPrevious</Title>
    <Subtitle>The section where you place the previous page button.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>PaginationNext</Title>
    <Subtitle>The section where you place the next page button.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>PaginationList</Title>
    <Subtitle>The section where you display page items.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>PaginationLink</Title>
    <Subtitle>A pagination link item.</Subtitle>
    <PropsTable defaultAsValue={'a'} rows={linkRows} />
    <br />
    <Title>PaginationEllipsis</Title>
    <Subtitle>The three dots range separator.</Subtitle>
    <PropsTable includeAsProp={false} rows={ellipsisRows} />
    <br />
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <Pagination rounded alignment={'centered'}>
          <Pagination.Previous>Previous</Pagination.Previous>
          <Pagination.Next>Next</Pagination.Next>
          <Pagination.List>
            <Pagination.Link>1</Pagination.Link>
            <Pagination.Ellipsis />
            <Pagination.Link>2</Pagination.Link>
          </Pagination.List>
        </Pagination>
      </Column>
    </Columns>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'classic-pagination-example'} height={'300px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};