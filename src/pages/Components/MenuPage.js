import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {EDITOR_OPTIONS} from "../../util";

const example = `import React from 'react';
import {Menu} from 'tenpines-bulma-react';

<Menu>
  <Menu.Label>General</Menu.Label>
  <Menu.List>
    <li><a>Dashboard</a></li>
    <li><a>Customers</a></li>
  </Menu.List>
  <Menu.Label>Administration</Menu.Label>
  <Menu.List>
    <li><a>Teams</a></li>
    <li><a>Users</a></li>
  </Menu.List>
</Menu>`;

export const MenuPage = () => {
  return <React.Fragment>
    <Title>Menu</Title>
    <Subtitle>A simple menu, for any type of vertical navigation.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>MenuLabel</Title>
    <Subtitle>A label to group your menu items.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>MenuList</Title>
    <Subtitle>A list wrapping your menu list items.</Subtitle>
    <PropsTable defaultAsValue={'ul'} />
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'dropdown-example'} height={'300px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};