import React from 'react';
import {Column, Columns, Subtitle, Tabs, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";
import AceEditor from "react-ace";

const tabsRows = [
  buildPropsRow('ulClassName', TYPES.STRING, '', 'Classes to append to the <code>ul</code> tabs element.'),
  buildPropsRow('alignment', TYPES.STRING, '', 'Sets the pagination alignment.'),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('tabs')),
  buildPropsRow('boxed', TYPES.BOOLEAN, '', booleanClassMessage('boxed')),
  buildPropsRow('rounded', TYPES.BOOLEAN, '', booleanClassMessage('rounded')),
  buildPropsRow('fullwidth', TYPES.BOOLEAN, '', booleanClassMessage('fullwidth')),
  buildPropsRow('toggle', TYPES.BOOLEAN, '', booleanClassMessage('toggle')),
  buildPropsRow('toggleRounded', TYPES.BOOLEAN, '', booleanClassMessage('toggleRounded'))
];

const example = `import React from 'react';
import {Tabs} from 'tenpines-bulma-react'

export const TabsExample = () => {
return <Tabs toggle toggleRounded>
  <Tabs.Item>Pictures</Tabs.Item>
  <Tabs.Item active>Music</Tabs.Item>
  <Tabs.Item>Videos</Tabs.Item>
</Tabs>
}`;

export const TabsPage = () => {
  return <React.Fragment>
    <Title>Tabs</Title>
    <Subtitle>Simple responsive horizontal navigation tabs, with different styles.</Subtitle>
    <PropsTable includeAsProp={false} rows={tabsRows} />
    <br/>
    <Title>TabsItem</Title>
    <Subtitle>A simple tab item.</Subtitle>
    <PropsTable />
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <Tabs toggle toggleRounded>
          <Tabs.Item>Pictures</Tabs.Item>
          <Tabs.Item active>Music</Tabs.Item>
          <Tabs.Item>Videos</Tabs.Item>
        </Tabs>
      </Column>
    </Columns>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'classic-tabs-example'} height={'200px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};