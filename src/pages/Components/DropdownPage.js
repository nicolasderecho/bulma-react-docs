import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, TYPES} from "../../util";

const example = `import React from 'react';
import {Dropdown} from 'tenpines-bulma-react';

export default class DropdownExample extends React.Component {

    state = {opened: false};
    
    render() {
      return <Dropdown active={this.state.opened}>
        <Dropdown.Trigger onClick={ () => this.setState(prevState => ({opened: !prevState.opened}))}>
          <button>Dropdown</button>
        </Dropdown.Trigger>
        <Dropdown.Menu>
          <Dropdown.Content>
            <Dropdown.Item>An item</Dropdown.Item>
            <Dropdown.Item>Another item</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Item>item after divider</Dropdown.Item>            
          </Dropdown.Content>
        </Dropdown.Menu>
      </Dropdown>    
    }
}`;

const dropdownRows = [
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active')),
  buildPropsRow('up', TYPES.BOOLEAN, '', booleanClassMessage('up')),
  buildPropsRow('right', TYPES.BOOLEAN, '', booleanClassMessage('right')),
  buildPropsRow('hoverable', TYPES.BOOLEAN, '', booleanClassMessage('hoverable'))
];

const itemRows = [
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active'))
];

export const DropdownPage = () => {
  return <React.Fragment>
    <Title>Dropdown</Title>
    <Subtitle>An interactive dropdown menu for discoverable content.</Subtitle>
    <PropsTable includeAsProp={false} rows={dropdownRows} />
    <br />
    <Title>DropdownTrigger</Title>
    <Subtitle>The dropdown section where you place the trigger, usually a button.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>DropdownMenu</Title>
    <Subtitle>The dropdown section where you place the menu that is displayed when the dropdown opens.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>DropdownContent</Title>
    <Subtitle>The dropdown content that is inside the menu.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>DropdownItem</Title>
    <Subtitle>An item inside the dropdown menu list.</Subtitle>
    <PropsTable defaultAsValue={'a'} rows={itemRows} />
    <br />
    <Title>DropdownDivider</Title>
    <Subtitle>A divider to separate menu items.</Subtitle>
    <PropsTable includeAsProp={false} />
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'dropdown-example'} height={'450px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};