import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, colorClassMessage, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";
import AceEditor from "react-ace";

const example = `import React from 'react';
import {Message} from 'tenpines-bulma-react';

<Message>
    <Message.Header>Title</Message.Header>
    <Message.Body>
        Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet
    </Message.Body>
</Message>`;

const example2 = `import React from 'react';
import {Message} from 'tenpines-bulma-react';

<Message>
    <Message.Body>
        This message doesn't contain a header so you will see styled it a little different.
    </Message.Body>
</Message>`;

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('message')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('message'))
];

export const MessagePage = () => {
  return <React.Fragment>
    <Title>Message</Title>
    <Subtitle>Colored message blocks, to emphasize part of your page.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
    <br />
    <Title>MessageHeader</Title>
    <Subtitle>The header section of the message.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>MessageBody</Title>
    <Subtitle>The body section of the message.</Subtitle>
    <PropsTable includeAsProp={false} />
    <Subtitle>Basic Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'message-example'} height={'200px'} value={example} />
      </Column>
    </Columns>
    <Subtitle>Message without header</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'message-without-header-example'} height={'190px'} value={example2} />
      </Column>
    </Columns>
  </React.Fragment>
};