import React, { useState } from 'react';
import {Subtitle, Title, Button, Modal, Box, Columns, Column, Help} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";
import AceEditor from "react-ace";

const rows = [
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active')),
  buildPropsRow('clipped', TYPES.BOOLEAN, '', `Used to determine whether to add <code>is-clipped</code> class to the <code>html</code> element or not.`),
  buildPropsRow('closeOnEscape', TYPES.BOOLEAN, '', `Used to determine whether to close the modal when you hit <code>ESC</code> key or not.`),
  buildPropsRow('closeFunction', TYPES.FUNCTION, '', 'Function that will be called to close the modal. <b>Needed</b> if you set <code>closeOnEscape</code> prop.'),
  buildPropsRow('onOpen', TYPES.FUNCTION, '', 'Function that will be called on modal opening'),
  buildPropsRow('onClose', TYPES.FUNCTION, '', 'Function that will be called on modal closing'),
  buildPropsRow('onMount', TYPES.FUNCTION, '', 'Function that will be called on modal mounting'),
  buildPropsRow('onUnmount', TYPES.FUNCTION, '', 'Function that will be called on modal unmounting')
];

const closeRows = [buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('close button'))];

const example = `import React, {useState} from 'react';
import {Modal, Box, Button} from 'tenpines-bulma-react';

export const ModalExample = () => {

const [openModal, setOpenModal] = useState(false);
const toggleModal = () => setOpenModal(!openModal);
const closeModal  = () => setOpenModal(false);

return <React.Fragment>
  <Button onClick={toggleModal} >Open modal</Button>
  
  <Modal active={openModal} closeFunction={closeModal} onClose={() => console.log('Closing modal')} closeOnEscape >
    <Modal.Background />
    <Modal.Content>
      <Box>Example of a simple Bulma modal.</Box>
    </Modal.Content>
    <Modal.Close size={'large'} onClick={closeModal} />
  </Modal>
</React.Fragment>
}`;

const example2 = `import React, {useState} from 'react';
import {Modal, Box, Button} from 'tenpines-bulma-react';

export const ClassicModalExample = () => {

  const [openClassicModal, setOpenClassicModal] = useState(false);
  const toggleClassicModal = () => setOpenClassicModal(!openClassicModal);
  const closeClassicModal  = () => setOpenClassicModal(false);

return <React.Fragment>
    <Button onClick={toggleClassicModal} >Classic modal</Button>
    
    <Modal active={openClassicModal} closeFunction={closeClassicModal} onClose={() => console.log('Closing classic modal')} closeOnEscape >
      <Modal.Background />
      <Modal.Card>
        <Modal.Card.Head>
          <Modal.Card.Title>Modal Title</Modal.Card.Title>
        </Modal.Card.Head>
        <Modal.Card.Body>
          <Box>Example of a classic Bulma modal.</Box>
        </Modal.Card.Body>
        <Modal.Card.Foot>
          <Button color={'success'}>Save</Button>
          <Button color={'danger'} onClick={closeClassicModal}>Cancel</Button>
        </Modal.Card.Foot>
      </Modal.Card>
    </Modal>
</React.Fragment>
}`;

export const ModalPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const toggleModal = () => setOpenModal(!openModal);
  const closeModal  = () => setOpenModal(false);

  const [openClassicModal, setOpenClassicModal] = useState(false);
  const toggleClassicModal = () => setOpenClassicModal(!openClassicModal);
  const closeClassicModal  = () => setOpenClassicModal(false);
  const hooksWarning = 'This example used react hooks just for simplicity. You can use the common React classes if you want.';

  return <React.Fragment>
    <Title>Modal</Title>
    <Subtitle>A classic modal overlay, in which you can include any content you want.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
    <br />
    <Title>ModalBackground</Title>
    <Subtitle>The common modal shadow background.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>ModalContent</Title>
    <Subtitle>The modal content section.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>ModalClose</Title>
    <Subtitle>The modal close button.</Subtitle>
    <PropsTable includeAsProp={false} rows={closeRows} />
    <br/>
    <Title>ModalCard</Title>
    <Subtitle>A modal card to display the modal with a classic design.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>ModalCardHead</Title>
    <Subtitle>The head section of the ModalCard.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>ModalCardTitle</Title>
    <Subtitle>The Title section of the ModalCard.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>ModalCardBody</Title>
    <Subtitle>The body section of the ModalCard.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>ModalCardFoot</Title>
    <Subtitle>The footer section of the ModalCard.</Subtitle>
    <PropsTable includeAsProp={false} />
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <Help>{hooksWarning}</Help>
        <AceEditor {...EDITOR_OPTIONS} name={'basic-modal-example'} height={'400px'} value={example} />
      </Column>
    </Columns>
    <Button onClick={toggleModal} >Open modal</Button>
    <Modal active={openModal} closeFunction={closeModal} onClose={() => console.log('Closing modal')} closeOnEscape >
      <Modal.Background />
      <Modal.Content>
        <Box>Example of a simple Bulma modal.</Box>
      </Modal.Content>
      <Modal.Close size={'large'} onClick={closeModal} />
    </Modal>
    <Subtitle style={{marginTop: '50px'}}>Classic Modal Example</Subtitle>
    <Columns>
      <Column>
        <Help>{hooksWarning}</Help>
        <AceEditor {...EDITOR_OPTIONS} name={'classic-modal-example'} height={'550px'} value={example2} />
      </Column>
    </Columns>
    <Button onClick={toggleClassicModal} >Classic modal</Button>
    <Modal active={openClassicModal} closeFunction={closeClassicModal} onClose={() => console.log('Closing classic modal')} closeOnEscape >
      <Modal.Background />
      <Modal.Card>
        <Modal.Card.Head>
          <Modal.Card.Title>Modal Title</Modal.Card.Title>
        </Modal.Card.Head>
        <Modal.Card.Body>
          <Box>Example of a classic Bulma modal.</Box>
        </Modal.Card.Body>
        <Modal.Card.Foot>
          <Button color={'success'}>Save</Button>
          <Button color={'danger'} onClick={closeClassicModal}>Cancel</Button>
        </Modal.Card.Foot>
      </Modal.Card>
    </Modal>

  </React.Fragment>;
};