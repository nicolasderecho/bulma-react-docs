import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, TYPES} from "../../util";

const example = `import React from 'react';
import {Card, CardHeader, CardFooterItem} from 'tenpines-bulma-react';

<Card>
  <Card.Header>
    //This could also be applied with <CardHeaderTitle>
    <CardHeader.Title centered>some text</CardHeader.Title>
  </Card.Header>
  <Card.Content>
    Some card content
  </Card.Content>
  <Card.Footer>
    <CardFooterItem>Save</CardFooterItem>
    <CardFooterItem>Edit</CardFooterItem>
    <CardFooterItem>Delete</CardFooterItem>
  </Card.Footer>
</Card>`;

const cardHeaderRows = [
  buildPropsRow('centered', TYPES.BOOLEAN, '', booleanClassMessage('centered'))
];

export const CardPage = () => {
  return <React.Fragment>
    <Title>Card</Title>
    <Subtitle>An all-around flexible and composable component.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>CardHeader</Title>
    <Subtitle>The card section where you place the header.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>CardHeaderTitle</Title>
    <Subtitle>The card header section where you place the title.</Subtitle>
    <PropsTable defaultAsValue={'p'} rows={cardHeaderRows} />
    <br />
    <Title>CardHeaderIcon</Title>
    <Subtitle>A wrapper inside the card header section to place an icon.</Subtitle>
    <PropsTable defaultAsValue={'span'} />
    <br />
    <Title>CardImage</Title>
    <Subtitle>The card section where you place the image.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>CardContent</Title>
    <Subtitle>The card section where you place the content.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>CardFooter</Title>
    <Subtitle>The card section where you place the footer.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br />
    <Title>CardFooterItem</Title>
    <Subtitle>An item inside the footer section.</Subtitle>
    <PropsTable defaultAsValue={'p'} />
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'card-example'} height={'330px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};