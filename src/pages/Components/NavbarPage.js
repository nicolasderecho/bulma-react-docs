import React from 'react';
import {Subtitle, Title, Navbar, Button, Buttons, Columns, Column} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, EDITOR_OPTIONS, TYPES} from "../../util";
import AceEditor from "react-ace";

const rows = [
  buildPropsRow('fixedTop', TYPES.BOOLEAN, '', booleanClassMessage('fixed-top')),
  buildPropsRow('fixedBottom', TYPES.BOOLEAN, '', booleanClassMessage('fixed-bottom')),
  buildPropsRow('transparent', TYPES.BOOLEAN, '', booleanClassMessage('transparent')),
  buildPropsRow('spaced', TYPES.BOOLEAN, '', booleanClassMessage('spaced')),
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('navbar'))
];

const activeRows = [
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active'))
];

const itemRows = [
  buildPropsRow('component', TYPES.COMPONENT, '', 'defines the React component that will be used to render the content.'),
  buildPropsRow('expanded', TYPES.BOOLEAN, '', booleanClassMessage('expanded')),
  buildPropsRow('tab', TYPES.BOOLEAN, '', booleanClassMessage('tab')),
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active')),
  buildPropsRow('hoverable', TYPES.BOOLEAN, '', booleanClassMessage('hoverable')),
  buildPropsRow('dropdown', TYPES.BOOLEAN, '', booleanClassMessage('dropdown', {prefix: 'has'})),
  buildPropsRow('dropdownUp', TYPES.BOOLEAN, '', booleanClassMessage('dropdown-up', {prefix: 'has'}))
];

const linkRows = [
  buildPropsRow('arrowless', TYPES.BOOLEAN, '', booleanClassMessage('arrowless'))
];

const dropdownRows = [
  buildPropsRow('right', TYPES.BOOLEAN, '', booleanClassMessage('right'))
];

const example = `import React from 'react';
import {Navbar, Buttons, Button} from 'tenpines-bulma-react';

export const NavbarExample = () => {

  const [openClassicModal, setOpenClassicModal] = useState(false);
  const toggleClassicModal = () => setOpenClassicModal(!openClassicModal);
  const closeClassicModal  = () => setOpenClassicModal(false);

return <Navbar>
      <Navbar.Brand>
        <Navbar.Item href={"https://bulma.io"} target={'_blank'} >
          <img src={"https://bulma.io/images/bulma-logo.png"} width="112" height="28" />
        </Navbar.Item>
        <Navbar.Burger />
      </Navbar.Brand>
      <Navbar.Menu>
        <Navbar.Start>
          <Navbar.Item>Home</Navbar.Item>
          <Navbar.Item>Documentation</Navbar.Item>
          <Navbar.Item dropdown hoverable>
            <Navbar.Link>More</Navbar.Link>
            <Navbar.Dropdown>
              <Navbar.Item>About</Navbar.Item>
              <Navbar.Item>Jobs</Navbar.Item>
              <Navbar.Divider />
              <Navbar.Item>Contact</Navbar.Item>
            </Navbar.Dropdown>
          </Navbar.Item>
        </Navbar.Start>
        <Navbar.End>
          <Navbar.Item>
            <Buttons>
              <Button>A button</Button>
            </Buttons>
          </Navbar.Item>
        </Navbar.End>
      </Navbar.Menu>
    </Navbar>
}`;

export const NavbarPage = () => {
  return <React.Fragment>
      <Title>Navbar</Title>
      <Subtitle>A responsive horizontal navbar that can support images, links, buttons, and dropdowns.</Subtitle>
      <PropsTable includeAsProp={false} rows={rows} />
      <br />
      <Title>NavbarBrand</Title>
      <Subtitle>A navbar brand container that is located on the left side.</Subtitle>
      <PropsTable includeAsProp={false} />
      <br />
      <Title>NavbarBurger</Title>
      <Subtitle>The navbar icon that will appear in small devices in order to show the items.</Subtitle>
      <PropsTable includeAsProp={false} rows={activeRows} />
      <br />
      <Title>NavbarMenu</Title>
      <Subtitle>The navbar menu section where you place your items.</Subtitle>
      <PropsTable includeAsProp={false} rows={activeRows} />
      <br />
      <Title>NavbarStart</Title>
      <Subtitle>One of the two direct and only children of the navbar-menu that will appear on the left.</Subtitle>
      <PropsTable includeAsProp={false}/>
      <br />
      <Title>NavbarEnd</Title>
      <Subtitle>One of the two direct and only children of the navbar-menu that will appear on the right.</Subtitle>
      <PropsTable includeAsProp={false} />
      <br />
      <Title>NavbarItem</Title>
      <Subtitle>An item that can be repeatable inside your navbar menu.</Subtitle>
      <PropsTable defaultAsValue={'a'} rows={itemRows} />
      <br />
      <Title>NavbarDropdown</Title>
      <Subtitle>A common Dropdown menu.</Subtitle>
      <PropsTable includeAsProp={false} rows={dropdownRows} />
      <br />
      <Title>NavbarLink</Title>
      <Subtitle>An arrow to display in your navbar dropdown.</Subtitle>
      <PropsTable includeAsProp={false} rows={linkRows} />
      <br />
      <Title>NavbarDivider</Title>
      <Subtitle>A divider to separate navbar items.</Subtitle>
      <PropsTable includeAsProp={false} />
      <br />
    <Subtitle>Example</Subtitle>
    <Navbar>
      <Navbar.Brand>
        <Navbar.Item href={"https://bulma.io"} target={'_blank'} >
          <img src={"https://bulma.io/images/bulma-logo.png"} width="112" height="28" alt={''} />
        </Navbar.Item>
        <Navbar.Burger />
      </Navbar.Brand>
      <Navbar.Menu>
        <Navbar.Start>
          <Navbar.Item>Home</Navbar.Item>
          <Navbar.Item>Documentation</Navbar.Item>
          <Navbar.Item dropdown hoverable>
            <Navbar.Link>More</Navbar.Link>
            <Navbar.Dropdown>
              <Navbar.Item>About</Navbar.Item>
              <Navbar.Item>Jobs</Navbar.Item>
              <Navbar.Divider />
              <Navbar.Item>Contact</Navbar.Item>
            </Navbar.Dropdown>
          </Navbar.Item>
        </Navbar.Start>
        <Navbar.End>
          <Navbar.Item>
            <Buttons>
              <Button>A button</Button>
            </Buttons>
          </Navbar.Item>
        </Navbar.End>
      </Navbar.Menu>
    </Navbar>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'classic-navbar-example'} height={'750px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};