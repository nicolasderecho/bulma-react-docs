import React from 'react';
import {Column, Columns, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";
import AceEditor from "react-ace";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('breadcrumb')),
  buildPropsRow('ulClassName', TYPES.STRING, '', 'Sets the classes to add to the ul that contains the breadcrumb items.'),
  buildPropsRow('alignment', TYPES.STRING, '', 'Sets the breadcrumb alignment.'),
  buildPropsRow('separator', TYPES.STRING, '', 'Sets the breadcrumb separator.')
];

const itemRows = [
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active'))
];

const example = `import React from 'react';
import {Breadcrumb, BreadcrumbItem} from 'tenpines-bulma-react';

<Breadcrumb size={'large'} separator={'dot'}>
  //This can be also applied using <Breadcrumb.Item>
  <BreadcrumbItem><a>Apples</a></BreadcrumbItem>
  <BreadcrumbItem active><a>Oranges</a></BreadcrumbItem>
  //Bulma's breadcrumb needs the list items to have a link to style them properly.
  <BreadcrumbItem><a>Bananas</a></BreadcrumbItem>
</Breadcrumb>`;

export const BreadcrumbPage = () => {
  return <React.Fragment>
    <Title>Breadcrumb</Title>
    <Subtitle>A simple breadcrumb component to improve your navigation experience.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows}/>
    <br/>
    <Title>BreadcrumbItem</Title>
    <Subtitle>A react shortcut to render a breadcrumb li item.</Subtitle>
    <PropsTable includeAsProp={false} rows={itemRows}/>
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'breadcrumb-example'} height={'200px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};