import React from 'react';
import {Subtitle, Title, Columns, Column, Panel} from "tenpines-bulma-react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, EDITOR_OPTIONS, TYPES} from "../../util";
import { faBook } from '@fortawesome/free-solid-svg-icons'
import AceEditor from "react-ace";

const blockRows = [
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active'))
];

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('panel'))
];

const example = `import React from 'react';
import {Panel} from 'tenpines-bulma-react'

export const PanelExample = () => {
return <Panel>
  <Panel.Heading>Repositories</Panel.Heading>
  <Panel.Block active>
    <Panel.Icon>
      <FontAwesomeIcon icon={faBook} />
    </Panel.Icon>
    Bulma
  </Panel.Block>
  <Panel.Block>
    <Panel.Icon>
      <FontAwesomeIcon icon={faBook} />
    </Panel.Icon>
    Github
  </Panel.Block>
  <Panel.Block>
    <Panel.Icon>
      <FontAwesomeIcon icon={faBook} />
    </Panel.Icon>
    10Pines
  </Panel.Block>
</Panel>}`;

export const PanelPage = () => {
  return <React.Fragment>
    <Title>Panel</Title>
    <Subtitle>A composable panel, for compact controls.</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
    <br/>
    <Title>PanelHeading</Title>
    <Subtitle>The heading section of the panel.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>PanelBlock</Title>
    <Subtitle>The content of the panel.</Subtitle>
    <PropsTable defaultAsValue={'div'} rows={blockRows} />
    <br/>
    <Title>PanelIcon</Title>
    <Subtitle>A wrapper to place a panel icon.</Subtitle>
    <PropsTable includeAsProp={false} />
    <br/>
    <Title>PanelTabs</Title>
    <Subtitle>A section to display tabs for navigation.</Subtitle>
    <PropsTable includeAsProp={false} />
    <Columns>
      <Column>
        <Panel>
          <Panel.Heading>Repositories</Panel.Heading>
          <Panel.Block active>
            <Panel.Icon>
              <FontAwesomeIcon icon={faBook} />
            </Panel.Icon>
            Bulma
          </Panel.Block>
          <Panel.Block>
            <Panel.Icon>
              <FontAwesomeIcon icon={faBook} />
            </Panel.Icon>
            Github
          </Panel.Block>
          <Panel.Block>
            <Panel.Icon>
              <FontAwesomeIcon icon={faBook} />
            </Panel.Icon>
            10Pines
          </Panel.Block>
        </Panel>
      </Column>
    </Columns>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'classic-panel-example'} height={'475px'} value={example} />
      </Column>
    </Columns>
  </React.Fragment>;
};