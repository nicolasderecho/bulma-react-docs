import React from 'react';
import {Box, Column, Columns, Title, Subtitle} from "tenpines-bulma-react";
import AceEditor from "react-ace";
import {EDITOR_OPTIONS} from "../../util";

const example1=`import React from 'react';
import 'bulma/bulma.sass';
import { Box } from "tenpines-bulma-react";

export const App = () => {
  return <Box>Hello World</Box>;
}
`;

const styles=`@import './variables';
@import '~bulma';`;

const cssVariables=`// Set your brand colors
$purple: #8A4D76;
$pink: #FA7C91;
$brown: #757763;
$beige-light: #D0D1CD;
$beige-lighter: #EFF0EB;

// Update Bulma's global variables
$family-sans-serif: "Nunito", sans-serif;
$grey-dark: $brown;
$grey-light: $beige-light;
$primary: $purple;
$link: $pink;
$widescreen-enabled: false;
$fullhd-enabled: false;

// Update some of Bulma's component variables
$body-background-color: $beige-lighter;
$control-border-width: 2px;
$input-border-color: transparent;
$input-shadow: none;`;

const customExample = `import React from 'react';
import './styles.scss';
import { Button } from "tenpines-bulma-react";

export const App = () => {
  return <Button color="primary">Button with edited color</Button>;
}
`;

export const GettingStarted = () => {
  return <React.Fragment>
    <Title>Getting started</Title>
    <Box>
      <p>Once you have installed <b>Bulma</b> and <b>tenpines-bulma-react</b> you only need to include Bulma CSS in order to start using the React components.</p>
    </Box>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'example'} value={example1} height={'150px'} />
      </Column>
    </Columns>
    <br/>
    <Title>Overriding Bulma's variables</Title>
    <Box>
      <p>If you want to override the variables defined by Bulma you can do it as you would normally do</p>
    </Box>
    <Columns multiline>
      <Column columnSize={'full'}>
        <Subtitle>variables.scss</Subtitle>
        <AceEditor {...EDITOR_OPTIONS} name={'variables'} value={cssVariables} height={'400px'} />
      </Column>
      <Column columnSize={'full'}>
        <Subtitle>styles.scss</Subtitle>
        <AceEditor {...EDITOR_OPTIONS} name={'styles'} value={styles} height={'50px'} />
      </Column>
      <Column columnSize={'full'}>
        <Subtitle>App.js</Subtitle>
        <AceEditor {...EDITOR_OPTIONS} name={'customExample'} value={customExample} height={'150px'} />
      </Column>
    </Columns>
  </React.Fragment>;
};