import React from 'react';
import {Title, Content} from "tenpines-bulma-react";

export const ExtensionsInstallation = () => {
  return <React.Fragment>
    <Title>Bulma Extensions</Title>
    <Content>
      Besides all the bulma styles to build your components, there are some <a href={'https://bulma.io/extensions/'} target={'_blank'}>Bulma Extensions</a> that basically allow you to create even more components!<br/>
      These are additions to Bulma, so in these cases you will need to also include the extra CSS in order to make it work.
    </Content>
  </React.Fragment>;
};