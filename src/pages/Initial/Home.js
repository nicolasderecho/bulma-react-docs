import React from 'react';
import {Box, Columns, Title, Column, Button, Notification} from "tenpines-bulma-react";
import AceEditor from 'react-ace';
import {EDITOR_OPTIONS} from "../../util";

const example1 = `import React from 'react';
import {Box, Button} from 'tenpines-bulma-react';

<Box>
  <Button>Ta da 🎉 </Button>
</Box>`;

export const Home = () => {
    return <React.Fragment>
        <Title>Welcome!</Title>
        <p><b>tenpines-react-bulma</b> is a set of React components for Bulma.</p>

        <p>This library only generates React components with the proper styles defined by Bulma (It also generates some of the components from the <a href={'https://bulma.io/extensions/'} target={'_blank'}>Bulma Extensions</a>). So if you never played with it, you should probably start taking a look at <a href={'https://bulma.io/documentation/'} target={'_blank'}>Bulma</a> first</p>

        <br/>
        <Title>Installation</Title>
        <Box>
            <code>$ npm install tenpines-bulma-react</code>
        </Box>
        <br/>
        <Title>Getting Started</Title>
        <p>Once you have installed <b>tenpines-react-bulma</b> you can just start importing components in your project:</p>
        <Columns>
            <Column><AceEditor {...EDITOR_OPTIONS } name={'getting-started'} height={'150px'} value={example1}/></Column>
        </Columns>

        <Box><Button>Ta da<span role={'img'} aria-label={'example icon'}> 🎉 </span></Button></Box>

        <Columns>
            <Column>
                <Notification color={'warning'}><b>tenpines-bulma-react</b> only adds the JS components for the Bulma styles. So <b>you will have to import Bulma CSS</b> by yourself in your project in order to make it work. The only components that also include their CSS are the ones that were extracted from <a href={'https://bulma.io/extensions/'} target={'_blank'}>Bulma Extensions</a></Notification>
            </Column>
        </Columns>
    </React.Fragment>;
};