import React from 'react';
import {Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, bulmaPropMessage, TYPES} from "../../util";

const rows = [
    buildPropsRow('hierarchy', TYPES.STRING, '', bulmaPropMessage('tile', 'hierarchy')),
    buildPropsRow('horizontalSize', TYPES.NUMBER, '', bulmaPropMessage('Tile', 'horizontal size')),
    buildPropsRow('vertical', TYPES.BOOLEAN, '', booleanClassMessage('vertical')),
];

export const TilePage = () => {
    return <React.Fragment>
        <Title>Tiles</Title>
        <Subtitle>A single tile element to build 2-dimensional Metro-like, Pinterest-like, or whatever-you-like grids.</Subtitle>
        <PropsTable rows={rows} />
    </React.Fragment>;
};