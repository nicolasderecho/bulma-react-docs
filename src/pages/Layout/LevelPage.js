import React from 'react';
import {Column, Columns, Subtitle, Title, Level, Field, Control, Input, Button, LevelItem} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {EDITOR_OPTIONS} from "../../util";

const basicExample = `import {Title, Level, LevelItem} from "tenpines-bulma-react";

<Level>
  <LevelItem className={'has-text-centered'}>
    <div>
      <p className={'is-uppercase'}>Tweets</p>
      <Title>3453</Title>
    </div>
  </LevelItem>
  <LevelItem className={'has-text-centered'}>
    <div>
      <p className={'is-uppercase'}>Following</p>
      <Title>123</Title>
    </div>
  </LevelItem>
  <LevelItem className={'has-text-centered'}>
    <div>
      <p className={'is-uppercase'}>Followers</p>
      <Title>452K</Title>
    </div>
  </LevelItem>
</Level>`;
const example = `import {Subtitle, Title, Level, Field, Control, Input, Button, LevelItem} from "tenpines-bulma-react";

<Level>
  <Level.Left>
    <Level.Item>
      <Subtitle as={'p'}><strong>123</strong> Posts</Subtitle>
    </Level.Item>
    <Level.Item>
      <Field addons>
        <Control><Input /></Control>
        <Control><Button>Search</Button></Control>
      </Field>
    </Level.Item>
  </Level.Left>
  <Level.Right>
    <LevelItem><Button>All</Button></LevelItem>
    <LevelItem><Button>Published</Button></LevelItem>
    <LevelItem><Button>Drafted</Button></LevelItem>
  </Level.Right>
</Level>`;

const imports = `import {Level, LevelRight, LevelLeft, LevelItem} from "tenpines-bulma-react"; //nested components can be imported separately

<Level>
  <Level.Left> //And they can also be used as children of the main component
    <Level.Item>
    </Level.Item>
  </Level.Left>
  <Level.Right>
    <LevelItem></LevelItem>
  </Level.Right>
</Level>`;

export const LevelPage = () => {
  return <React.Fragment>
    <Title>Level</Title>
    <p>A multi-purpose <b>horizontal</b> level, which can contain almost any other element.</p>
    <br/>
    <Subtitle>Props</Subtitle>
    <Columns>
      <Column>
        <PropsTable defaultAsValue={'nav'}/>
      </Column>
    </Columns>
    <Title>LevelLeft</Title>
    <p>The left side of the Level component</p>
    <br/>
    <Subtitle>Props</Subtitle>
    <Columns>
      <Column>
        <PropsTable/>
      </Column>
    </Columns>
    <Title>LevelRight</Title>
    <p>The right side of the Level component</p>
    <br/>
    <Subtitle>Props</Subtitle>
    <Columns>
      <Column>
        <PropsTable/>
      </Column>
    </Columns>
    <Title>LevelItem</Title>
    <p>Represents each individual item</p>
    <br/>
    <Subtitle>Props</Subtitle>
    <Columns>
      <Column>
        <PropsTable/>
      </Column>
    </Columns>
    <Subtitle>Imports</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'imports'} value={imports} height={'250px'} />
      </Column>
    </Columns>
    <Subtitle>Basic Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'level-basic'} value={basicExample} height={'450px'} />
      </Column>
    </Columns>
    <Columns>
      <Column>
        <Level>
          <LevelItem className={'has-text-centered'}>
            <div>
              <p className={'is-uppercase'}>Tweets</p>
              <Title>3453</Title>
            </div>
          </LevelItem>
          <LevelItem className={'has-text-centered'}>
            <div>
              <p className={'is-uppercase'}>Following</p>
              <Title>123</Title>
            </div>
          </LevelItem>
          <LevelItem className={'has-text-centered'}>
            <div>
              <p className={'is-uppercase'}>Followers</p>
              <Title>452K</Title>
            </div>
          </LevelItem>
        </Level>
      </Column>
    </Columns>

    <br/>
    <Subtitle>Example With Left and Right Section</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'level-left-right'} value={example} height={'400px'} />
      </Column>
    </Columns>
    <Columns>
      <Column>
        <Level>
          <Level.Left>
            <Level.Item>
              <Subtitle as={'p'}><strong>123</strong> Posts</Subtitle>
            </Level.Item>
            <Level.Item>
              <Field addons>
                <Control><Input /></Control>
                <Control><Button>Search</Button></Control>
              </Field>
            </Level.Item>
          </Level.Left>
          <Level.Right>
            <LevelItem><Button>All</Button></LevelItem>
            <LevelItem><Button>Published</Button></LevelItem>
            <LevelItem><Button>Drafted</Button></LevelItem>
          </Level.Right>
        </Level>
      </Column>
    </Columns>
  </React.Fragment>;
};