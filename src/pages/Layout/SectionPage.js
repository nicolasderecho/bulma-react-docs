import React from 'react';
import {Title, Subtitle} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, sizeClassMessage, TYPES} from "../../util";

const componentProps = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('Section'))
];

export const SectionPage = () => {
  return <React.Fragment>
    <Title>Section</Title>
    <Subtitle>A simple container to divide your page into <b>sections</b>, like the one you're currently reading.</Subtitle>
    <PropsTable defaultAsValue={'section'} rows={componentProps} />
  </React.Fragment>;
};