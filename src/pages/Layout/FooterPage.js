import React from 'react';
import {Title, Subtitle} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";

export const FooterPage = () => {
  return <React.Fragment>
    <Title>Footer</Title>
    <Subtitle>A simple responsive <b>footer</b> which can include anything: lists, headings, columns, icons, buttons, etc.</Subtitle>
    <PropsTable defaultAsValue={'footer'} />
  </React.Fragment>;
};