import React from 'react';
import {Title, Subtitle} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {booleanClassMessage, buildPropsRow, colorClassMessage, sizeClassMessage, TYPES} from "../../util";

const componentProps = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('Hero')),
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('Hero')),
  buildPropsRow('bold', TYPES.BOOLEAN, '', booleanClassMessage('bold')),
  buildPropsRow('fullheightWithNavbar', TYPES.BOOLEAN, '', booleanClassMessage('fullheight-with-navbar'))
];

export const HeroPage = () => {
  return <React.Fragment>
    <Title>Hero</Title>
    <Subtitle>An imposing <b>hero banner</b> to showcase something</Subtitle>
    <PropsTable defaultAsValue={'section'} rows={componentProps} />
  </React.Fragment>;
};