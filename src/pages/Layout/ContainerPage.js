import React from 'react';
import {Columns, Container, Subtitle, Title, Notification, Column} from "tenpines-bulma-react";
import AceEditor from 'react-ace';
import {booleanClassMessage, buildPropsRow, EDITOR_OPTIONS, TYPES} from "../../util";
import {PropsTable} from "../../components/shared/PropsTable";

const example1 = `import React from 'react';
import {Container, Notification} from 'tenpines-bulma-react';

<Container>
    <Notification>I'm a notification inside a container</Notification>
</Container>`;

const example2 = `import React from 'react';
import {Container, Notification} from 'tenpines-bulma-react';

<Container fluid>
    <Notification>This container is fluid: it will have a 32px gap on either side, on any viewport size.</Notification>
</Container>`;

const example3 = `import React from 'react';
import {Container, Notification} from 'tenpines-bulma-react';

<Container widescreen>
    <Notification>This container is fullwidth until the <code>$widescreen</code> breakpoint.</Notification>
</Container>`;

const componentProps = [
  buildPropsRow('fluid', TYPES.BOOLEAN, '', booleanClassMessage('fluid')),
  buildPropsRow('fullhd', TYPES.BOOLEAN, '', booleanClassMessage('fullhd')),
  buildPropsRow('widescreen', TYPES.BOOLEAN, '', booleanClassMessage('widescreen')),
];

export const ContainerPage = () => {
  return <React.Fragment>
    <Title>Container</Title>
    <p>A simple container to center your content horizontally. It can be used in any context.</p><br/>
    <Subtitle>Props</Subtitle>
    <Columns>
      <Column>
        <PropsTable rows={componentProps} />
      </Column>
    </Columns>
    <Subtitle>Basic example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'basic-container'} value={example1} height={'150px'} />
      </Column>
    </Columns>
    <Container>
      <Notification>I'm a notification inside a container</Notification>
    </Container>
    <br/>
    <Subtitle>Fluid container</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'fluid-container'} value={example2} height={'150px'} />
      </Column>
    </Columns>
    <Container fluid>
      <Notification>This container is fluid: it will have a 32px gap on either side, on any viewport size.</Notification>
    </Container>
    <br/>
    <Subtitle>Breakpoint container</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'breakpoint-container'} value={example3} height={'150px'} />
      </Column>
    </Columns>
    <Container widescreen>
      <Notification>This container is fullwidth until the <code>$widescreen</code> breakpoint.</Notification>
    </Container>
  </React.Fragment>;
};