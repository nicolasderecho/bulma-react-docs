import React from 'react';
import {Button, Column, Columns, Content, Image, Media, Subtitle, Title} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import AceEditor from "react-ace";
import {EDITOR_OPTIONS} from "../../util";

const example = `import {Button, Content, Image, Media, Subtitle, Title} from "tenpines-bulma-react";

<Media>
  <Media.Left as={'figure'}><Image is={'64x64'} src={'https://bulma.io/images/placeholders/128x128.png'}/></Media.Left>
  <Media.Content>
    <Content>
      <p>
        <strong>John Smith</strong> <small>@johnsmith</small> <small>31m</small>
        <br />
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. 
        Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
      </p>
    </Content>
  </Media.Content>
  <Media.Right>
    <Button color={'info'} className={'delete'} />
  </Media.Right>
</Media>`;

export const MediaObjectPage = () => {
  return <React.Fragment>
    <Title>Media</Title>
    <Subtitle>The famous media object prevalent in social media interfaces, but useful in any context.</Subtitle>
    <br />
    <Subtitle>Props</Subtitle>
    <Columns>
      <Column>
        <PropsTable defaultAsValue={'article'} />
      </Column>
    </Columns>
    <Subtitle>Example</Subtitle>
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'media-object'} value={example} height={'350px'} />
      </Column>
    </Columns>
    <Columns>
      <Column>
        <Media>
          <Media.Left as={'figure'}><Image is={'64x64'} src={'https://bulma.io/images/placeholders/128x128.png'}/></Media.Left>
          <Media.Content>
            <Content>
              <p>
                <strong>John Smith</strong> <small>@johnsmith</small> <small>31m</small>
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
              </p>
            </Content>
          </Media.Content>
          <Media.Right>
            <Button color={'info'} className={'delete'} />
          </Media.Right>
        </Media>
      </Column>
    </Columns>

  </React.Fragment>;
};