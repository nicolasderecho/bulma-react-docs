import React, {useState} from 'react';
import {Title, Subtitle, Button, Tooltip, Columns, Column, Notification} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {
  booleanClassMessage,
  buildPropsRow,
  colorClassMessage,
  EDITOR_OPTIONS,
  TYPES
} from "../../util";
import AceEditor from "react-ace";

const rows = [
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('tooltip')),
  buildPropsRow('text', TYPES.STRING, '', 'Sets the tooltip text'),
  buildPropsRow('position', TYPES.STRING, '', 'Sets the tooltip position'),
  buildPropsRow('positionMobile', TYPES.STRING, '', 'Sets the tooltip position on Mobile'),
  buildPropsRow('positionTablet', TYPES.STRING, '', 'Sets the tooltip position on Tablet'),
  buildPropsRow('positionTouch', TYPES.STRING, '', 'Sets the tooltip position on Touch'),
  buildPropsRow('positionDesktop', TYPES.STRING, '', 'Sets the tooltip position on Desktop'),
  buildPropsRow('positionWidescreen', TYPES.STRING, '', 'Sets the tooltip position on Widescreen'),
  buildPropsRow('positionFullhd', TYPES.STRING, '', 'Sets the tooltip position on Fullhd'),
  buildPropsRow('active', TYPES.BOOLEAN, '', booleanClassMessage('active')),
  buildPropsRow('multiline', TYPES.STRING, '', booleanClassMessage('multiline')),

];

const example = `import React, {useState} from 'react';
import { Button, Tooltip } from 'tenpines-bulma-react';
const [isActive, setActive] = useState(false);
<Tooltip text={'Look me! a tooltip'} color={'warning'} positionMobile={'left'} active={isActive} position={'bottom'}>Look</Tooltip>
<Button onClick={() => setActive(!isActive) }>Toogle Tooltip</Button>`;

export const TooltipPage = () => {
  const [isActive, setActive] = useState(false);
  return <React.Fragment>
    <Title>Tooltip</Title>
    <Subtitle>Display a tooltip attached to any kind of element with different positioning.</Subtitle>
    <Columns>
      <Column>
        <Notification color={'info'}>This component has been ported from the Bulma extension <a href={'https://wikiki.github.io/form/slider/'} target={'_blank'}>Wikiki Slider</a></Notification>
      </Column>
    </Columns>
    <PropsTable includeAsProp={false} rows={rows} />
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'slider-example'} height={'100px'} value={example} />
      </Column>
    </Columns>
    <Button onClick={() => setActive(!isActive) }>Toogle Tooltip</Button>
    <Tooltip text={'Look me! a tooltip'} color={'warning'} positionMobile={'left'} active={isActive} position={'bottom'}>Look</Tooltip>
  </React.Fragment>;
}