import React, {useState} from 'react';
import {Title, Subtitle, Slider, Columns, Column, Notification} from "tenpines-bulma-react";
import {PropsTable} from "../../components/shared/PropsTable";
import {buildPropsRow, colorClassMessage, EDITOR_OPTIONS, sizeClassMessage, TYPES} from "../../util";
import AceEditor from "react-ace";

const rows = [
  buildPropsRow('size', TYPES.STRING, '', sizeClassMessage('slider')),
  buildPropsRow('color', TYPES.STRING, '', colorClassMessage('slider')),
  buildPropsRow('orientation', TYPES.STRING, '', 'Used to determine whether to use a vertical or horizontal slider'),
  buildPropsRow('min', TYPES.STRING, '', 'Sets the minimum value of the range'),
  buildPropsRow('max', TYPES.STRING, '', 'Sets the maximum value of the range'),
  buildPropsRow('value', TYPES.STRING, '', 'Sets the current slider value'),
  buildPropsRow('step', TYPES.STRING, '', 'Sets how much the slider jumps on every change')
];

const example = `import React, {useState} from 'react';
import { Slider } from 'tenpines-bulma-react';
const [number, setNumber] = useState(50);
<Slider color={'warning'} size={'large'} min={0} max={100} step={1} value={number} onChange={(newValue) => setNumber(newValue)} />`;

export const SliderPage = () => {
  const [number, setNumber] = useState(50);
  return <React.Fragment>
    <Title>Slider</Title>
    <Subtitle>Display a classic slider with different colors, sizes, and states</Subtitle>
    <PropsTable includeAsProp={false} rows={rows} />
    <Columns>
      <Column>
        <AceEditor {...EDITOR_OPTIONS} name={'slider-example'} height={'100px'} value={example} />
      </Column>
    </Columns>
    <Slider color={'warning'} size={'large'} min={0} max={100} step={1} value={number} onChange={(newValue) => setNumber(newValue)} />
    <Columns>
      <Column>
        <Notification color={'info'}>This component has been ported from the Bulma extension <a href={'https://wikiki.github.io/form/slider/'} target={'_blank'}>Wikiki Slider</a></Notification>
      </Column>
    </Columns>
  </React.Fragment>;
};